BrainRuck
=========

Brainfuck to ruby and vice versa interpreter, and much more!


What is it?
-----------

BrainRuck is a brainfuck interpreter in ruby that also doubles as a converter. for instance,

```
$ ./brainfuck.rb -c 'hello world'

++++++++++[>++++++++++<-]>++++.[-]++++++++++[>++++++++++<-]>+.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>+++++++++++<-]>+.[-]++++++++++[>+++<-]>++.[-]++++++++++[>+++++++++++<-]>+++++++++.[-]++++++++++[>+++++++++++<-]>+.[-]++++++++++[>+++++++++++<-]>++++.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>++++++++++<-]>.[-]
```

stick that right back into the interpreter one of two ways:
```
$ ./brainfuck.rb

Brainfuck 1.0.2

++> ++++++++++[>++++++++++<-]>++++.[-]++++++++++[>++++++++++<-]>+.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>+++++++++++<-]>+.[-]++++++++++[>+++<-]>++.[-]++++++++++[>+++++++++++<-]>+++++++++.[-]++++++++++[>+++++++++++<-]>+.[-]++++++++++[>+++++++++++<-]>++++.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>++++++++++<-]>.[-]

=+> hello world

executed in 0.010309 sec(s)

++> 
```
or
```
$ ./brainfuck.rb -i '++> ++++++++++[>++++++++++<-]>++++.[-]++++++++++[>++++++++++<-]>+.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>+++++++++++<-]>+.[-]++++++++++[>+++<-]>++.[-]++++++++++[>+++++++++++<-]>+++++++++.[-]++++++++++[>+++++++++++<-]>+.[-]++++++++++[>+++++++++++<-]>++++.[-]++++++++++[>++++++++++<-]>++++++++.[-]++++++++++[>++++++++++<-]>.[-]'


hello world.
```
Other features:
===============


Running 'actual' brainfuck code:
--------------------------------

In this interpreter you may run brainfuck code in two ways.

1) ```$ ./brainfuck```

2) ```$ ./brainfuck scriptname.bf```

Simple enough right?



Converting ruby scripts to brainfuck and running them:
------------------------------------------------------

You may convert whole ruby scripts into the brainfuck language (providing the script is all one line, ready up your ";"!)

its as simple as this:

```./brainfuck -e script.rb > script.bf```

then, with the newly created brainfuck script, you may run it like this:

```./brainfuck --to-rb script.bf```




